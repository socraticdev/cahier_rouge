# cahier_rouge
Tiny app dedicated to display, add, and modifiy quotations.
I made it to digitize reading notes I had in a red notebook (aka 'Le Cahier rouge')

while at it, i added a 'syntagmas' feature to display and add syntagmas.

A syntagma is a collection of phonems, words, or sentences that have an atomic signification.

### installation
- create virtual environnement : python -venv myVirtualEnvName
- activate virtual environnement : source myVirtualEnvName/Scripts{or bin on linux}/activate
- install Flask : pip install -U Flask

#### before running make sure you assigne FLASK variables via the terminal
- export FLASK_APP=nameOfFile(without .py)
- export FLASK_ENV=development (to have debugging informations)

to run : ``flask run``

## running as a service (manual way)
```
source env/bin/activate
export FLASK_APP=cahier_rouge
flask run --host=0.0.0.0 &
jobs
disown -h %1 #or the index of the job from the list
``` 
