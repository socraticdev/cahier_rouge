function trigger(id) {
  let element = document.getElementById(id);

  if (element !== null) {
    element.click();
  }
}
