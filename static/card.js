document.onkeypress = function (e) {
  switch (e.key) {
    case "a":
      trigger("btnAddCard");
      break;
  }
};

document.onkeydown = function (e) {
  switch (e.key) {
    case "ArrowRight":
      trigger("btnNextCard");
      break;
    case "ArrowLeft":
      trigger("btnPreviousCard");
      break;
  }
};
