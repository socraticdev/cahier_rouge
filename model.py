"""
model.py
--------
Implements the model for our website by simulating a database.

Note: although this is nice as a simple example, don't do this in a real-world
production setting. Having a global object for application data is asking for
trouble. Instead, use a real database layer, like
https://flask-sqlalchemy.palletsprojects.com/.
"""

import json


def load_db(file_name):
    with open(file_name, encoding="utf8") as f:
        return json.load(f)

def save_db():
    with open("data.json", 'w') as f:
        return json.dump(db, f)

def save_syntagma():
    with open("syntagmas_data.json", 'w') as f:
        return json.dump(db_syntagmas, f)

db = load_db("data.json")
db_syntagmas = load_db("syntagmas_data.json")