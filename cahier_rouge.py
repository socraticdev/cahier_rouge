from flask import Flask, render_template, abort, jsonify, request, redirect, url_for
import uuid, datetime

from model import db, db_syntagmas, save_db, save_syntagma

app = Flask(__name__)


@app.route("/")
def welcome():
    return render_template(
        "welcome.html",
        cards=db
    )

@app.route("/syntagmas")
def syntagmas():
    return render_template(
        "syntagmas.html",
        cards=db_syntagmas
    )

@app.route("/add_syntagme", methods=["GET", "POST"])
def add_syntagme():
    if request.method == "POST":
        # form has been submitted, process data
        card = {"title": request.form['title'],
                "uuid": str(uuid.uuid1()),
                "date": str(datetime.datetime.now())}

        db_syntagmas.append(card)
        save_syntagma()
        return redirect(url_for('syntagmas'))
    else:
        return render_template("add_syntagma.html")

@app.route("/modify_card/<int:index>", methods=["GET", "POST"])
def modify_card(index):
    if request.method == "POST":
        for i in range(len(db)):
            if db[i]['uuid'] == request.form['uuid']:
                db.pop(i)
                break

        modifiedCard = {"title": request.form['title'],
                "author": request.form['author'],
                "quote": request.form['quote'],
                "page": request.form['page'],
                "uuid": request.form['uuid'],
                "date": str(datetime.datetime.now())}

        db.append(modifiedCard)
        save_db()
        return redirect(url_for('card_view', index=len(db)-1))
    else:
        try:    
            card = db[index]
            print(card)
            return render_template("modify_card.html", 
                                card=card,
                                index=index,
                                action="Edit")
        except IndexError:
            abort(404)

@app.route("/add_card", methods=["GET", "POST"])
def add_card():
    if request.method == "POST":
        # form has been submitted, process data
        card = {"title": request.form['title'],
                "author": request.form['author'],
                "quote": request.form['quote'],
                "page": request.form['page'],
                "uuid": str(uuid.uuid1()),
                "date": str(datetime.datetime.now())}

        db.append(card)
        save_db()
        return redirect(url_for('card_view', index=len(db)-1))
    else:
        return render_template("add_card.html")
            
@app.route("/card/<int:index>")
def card_view(index):
    try:    
        card = db[index]
        return render_template("card.html", 
                                card=card,
                                index=index,
                                max_index=len(db) - 1)
    except IndexError:
        abort(404)

@app.route("/api/card/<int:index>")
def api_card_detail(index):
    try:
        return db[index]
    except IndexError:
        abort(404)

@app.route("/api/cards/")
def api_cards_list():
    return jsonify(db)